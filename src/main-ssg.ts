import {createApp} from './app';
import App from './app/AppSsg.vue';
import {routes} from './pages/ssg';

/**
 * SSG client entry point
 */
const {app, router} = createApp({App, routes});

router
	.isReady()
	.then(() => app.mount('#app'));
