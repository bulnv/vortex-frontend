export interface IProfileStatsItem {
	value: number;
	title: string;
}

export enum EStats {
	RATING = 'rating',
	// SUBSCRIBERS = 'subscribers',
	// SUBSCRIPTIONS = 'subscriptions',
	// POSTS = 'posts',
	// TRENDS = 'trends',
	COMMENTS = 'comments',
	MINUSES = 'minuses',
	PLUSES = 'pluses',
}
