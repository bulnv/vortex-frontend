import {EStats} from '../ui/profile-stats-ui';

import type {IProfileStats} from './types';

export const defaultStats: IProfileStats = {
	stats: {
		[EStats.RATING]: {
			value: 1144,
			title: 'рейтинг'
		},
		// [EStats.SUBSCRIBERS]: {
		// 	value: 1149,
		// 	title: 'подписчиков'
		// },
		// [EStats.SUBSCRIPTIONS]: {
		// 	value: 1100,
		// 	title: 'подписок'
		// },
		// [EStats.POSTS]: {
		// 	value: 0,
		// 	title: 'постов'
		// },
		// [EStats.TRENDS]: {
		// 	value: 1150,
		// 	title: 'в трендах'
		// },
		[EStats.COMMENTS]: {
			value: 1150,
			title: 'коментариев'
		},
		[EStats.MINUSES]: {
			value: 140,
			title: 'минусов'
		},
		[EStats.PLUSES]: {
			value: 20,
			title: 'плюсов'
		},
	}
};
