import type {IProfileStatsItem,EStats} from '../ui/profile-stats-ui';

export interface IProfileStats {
	stats: Record<EStats, IProfileStatsItem>,
}

export interface IProfileStatsProps {
	rating?: number;
	comments?: number;
	pluses?: number;
	minuses?: number;
}

export interface IProfileInformation {
	name: string;
	registration: Date;
}

export interface IProfileHeader {
	registration: Date;
	name: string;
	status: string;
	avatar: string;
}
