// eslint-disable-next-line
import {PostStatusEnum} from "@/entities/post";

import {getAPI} from "@/shared/api";
import {APINames} from "@/shared/config";

import {responseToPostsPublished, argsToGetPostsParams} from "../lib/posts-mapper";
import type {PostsPublishedResponse, PostsDraftResponse, IGetPostsParams} from '../types';

export const getPublishedPosts = async (args?: IGetPostsParams): Promise<PostsPublishedResponse> => {
	const params = {
		...argsToGetPostsParams(args),
		status: PostStatusEnum.PUBLISHED,
	};

	const response =
		await getAPI(APINames.MONOLITH).getWithCredentials<PostsPublishedResponse>(`/posts/`, params);

	return {
		...response,
		results: responseToPostsPublished(response),
	};
};

export const getDraftPosts = async (args: IGetPostsParams): Promise<PostsDraftResponse> => {
	const params = {
		...argsToGetPostsParams(args),
		status: PostStatusEnum.DRAFT,
	};

	return await getAPI(APINames.MONOLITH).getWithCredentials(`/posts/`, params);
};
