import {getPublishedPosts, getDraftPosts} from "../api/posts-feed-api";
import {IGetPostsParamsKeys} from "../config";

export const createDraftFeedFetcher = (username: string) => {
	return async (cursor: string | null) => {
		return getDraftPosts({
			username,
			cursor,
		});
	}
}

export const createPublishedFeedFetcher = () => {
    return async (cursor: string | null) => {
        return getPublishedPosts({
            cursor,
        });
    }
}

export const createDiscussedFeedFetcher = () => {
    return async (cursor: string | null) => {
        return getPublishedPosts({
            cursor,
            [IGetPostsParamsKeys.FEED_TYPE]: 'commented',
        });
    }
}

export const createTopFeedFetcher = () => {
    return async (cursor: string | null) => {
        return getPublishedPosts({
            cursor,
            [IGetPostsParamsKeys.FEED_TYPE]: 'rating',
        });
    }
}

export const createUserFeedFetcher = (username: string) => {
    return async (cursor: string | null) => {
        return getPublishedPosts({
            username,
            cursor,
        });
    }
}


