import {PostsFeeds} from "../config";
import {useNewFeed, useUserFeed, useDiscussedFeed, useTopFeed} from "../model/posts-feeds-stores";

export const selectPublishedFeed = (name: PostsFeeds) => {
	switch (name) {
		case PostsFeeds.NEW: return useNewFeed();
		case PostsFeeds.USER: return useUserFeed();
		case PostsFeeds.DISCUSSED: return useDiscussedFeed();
		case PostsFeeds.TOP: return useTopFeed();
	}
}
