// eslint-disable-next-line
import type {PostPublished} from "@/entities/post";
import type {PostsPublishedResponse,IGetPostsParams} from "@/entities/posts-feed";

import {getTimeAgo} from "@/shared/lib/time-tools";


export const argsToGetPostsParams = (args: IGetPostsParams | undefined): Record<string, string> => {
    const params: Record<string, string> = {};

	if (!args)
		return {};

    Object.keys(args).forEach((key) => {
		const arg = args[key as keyof IGetPostsParams];

		if (arg) {
			params[key] = arg;
		}
    })

    return params;
}

export const responseToPostsPublished = (response: PostsPublishedResponse): PostPublished[] => {
    return response.results.map(post => {
        const publishedAtDate = new Date(post.published_at);

        return {
            ...post,
            published_at: publishedAtDate,
            time_ago: getTimeAgo(publishedAtDate),
        };
    });
}

