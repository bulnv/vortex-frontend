import {PostsFeeds} from "../config";
import {
	createPublishedFeedFetcher,
	createDiscussedFeedFetcher,
	createTopFeedFetcher
} from "../lib/posts-feeds-fetchers";

export const selectFetcher = (name: PostsFeeds) => {
	switch (name) {
		case PostsFeeds.TOP: return createTopFeedFetcher();
		case PostsFeeds.DISCUSSED: return createDiscussedFeedFetcher();
	}

	return createPublishedFeedFetcher();
}
