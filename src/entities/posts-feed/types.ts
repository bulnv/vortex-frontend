// eslint-disable-next-line
import {PostStatusEnum} from "@/entities/post";
// eslint-disable-next-line
import type {PostPublished, PostDraft} from '@/entities/post';
import {IGetPostsParamsKeys, PostsFeeds} from "./config";

export interface BasePostsResponse<T> {
	count: number;
	next: string;
	previous: string;
	results: T;
}

export interface IGetPostsParams {
	[IGetPostsParamsKeys.CURSOR]?: string | null;
	[IGetPostsParamsKeys.USERNAME]?: string | null;
	[IGetPostsParamsKeys.PERIOD]?: string | null;
	[IGetPostsParamsKeys.FEED_TYPE]?: string | null;
	[IGetPostsParamsKeys.TAG]?: PostsFeeds;
	[IGetPostsParamsKeys.STATUS]?: PostStatusEnum;
}

export type Fetcher<T> = (cursor: string | null) => Promise<T>;

export interface PostsDraftResponse extends BasePostsResponse<PostDraft[]> {}
export interface PostsPublishedResponse extends BasePostsResponse<PostPublished[]> {}
