export enum PostsFeeds {
    TRENDS = 'TRENDS',
    NEW = 'NEW',
    TOP = 'TOP',
    DISCUSSED = 'DISCUSSED',
    SUBSCRIPTIONS = 'SUBSCRIPTIONS',
    COPYRIGHT = 'COPYRIGHT',
    GROUPS = 'GROUPS',
    TAGS = 'TAGS',
	DRAFT = 'DRAFT',
	USER = 'USER',
}

export enum IGetPostsParamsKeys {
    CURSOR = 'cursor',
    USERNAME = 'username',
    PERIOD = 'period',
    TAG = 'tag',
    FEED_TYPE = 'feed_type',
    STATUS = 'status',
}
