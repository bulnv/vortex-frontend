import {watchEffect, watch} from "vue";

import {useDraftFeed, createDraftFeedFetcher} from "@/entities/posts-feed";
//TODO
// eslint-disable-next-line
import {useViewerStore} from "@/entities/user";

export const useDraftsPresenter = () => {
	const viewer = useViewerStore();
	const feed = useDraftFeed();

	const reload = () => {
		if (feed.posts.length)
			return;

		if (viewer.profile) {
			feed.reload(createDraftFeedFetcher(viewer.profile.username));
		}
	};

	watch(() => viewer.profile, reload);
	reload();

	return feed;
};
