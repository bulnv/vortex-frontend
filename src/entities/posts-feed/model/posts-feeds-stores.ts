import {defineStore} from "pinia";
import {ref, type Ref} from "vue";

//TODO
// eslint-disable-next-line
import {type IPost, PostStatusEnum} from "@/entities/post";

//TODO
// eslint-disable-next-line
import type {PostDraft, PostPublished} from "@/entities/post";
import {useRequest} from "@/shared/api";
import {DEFAULT_ERROR_MESSAGE} from "@/shared/config";

import type {BasePostsResponse, Fetcher} from "../types";


//TODO remove IPost
const createFeedStore = <Post extends IPost> (name: string) => {
	return defineStore(`${name}-feed-store`, () => {
		const {request, isLoading, error} = useRequest(DEFAULT_ERROR_MESSAGE);
		const posts = ref<Post[]>([]) as Ref<Post[]>;
		const isLastPage = ref(false);

		let cursor: string | null = null;

		const set = (newPosts: Post[]) => {
			posts.value = newPosts;
		};

		const add = (newPosts: Post[]) => {
			posts.value = [...newPosts, ...posts.value];
		};

		const cut = (slug: string, post?: Post) => {
			if (posts.value.length === 0)
				return;

			posts.value = posts.value.filter(post => post.slug !== slug);

			if (post) {
				add([post]);
			}
		}

		const upload = async (fetcher: Fetcher<BasePostsResponse<Post[]>>) => {
			if (isLastPage.value)
				return;

			const response = await request<BasePostsResponse<Post[]>>(() => fetcher(cursor));

			if (!response) {
				return;
			}

			if (response.results) {
				add(response.results as Post[]);
			}

			cursor = response.next;
			isLastPage.value = !response.next;
		}

		const reload = async (fetcher: Fetcher<BasePostsResponse<Post[]>>) => {
			set([]);
			isLastPage.value = false;
			cursor = null;
			await upload(fetcher);
		};

		return {
			posts, isLastPage,
			isLoading, error,
			upload, reload, cut
		};
	});
};

export const useDraftFeed =
	createFeedStore<PostDraft>(PostStatusEnum.DRAFT);
export const useTopFeed =
	createFeedStore<PostPublished>(PostStatusEnum.PUBLISHED);
export const useNewFeed =
	createFeedStore<PostPublished>(PostStatusEnum.PUBLISHED);
export const useUserFeed =
	createFeedStore<PostPublished>(PostStatusEnum.PUBLISHED);
export const useDiscussedFeed =
	createFeedStore<PostPublished>(PostStatusEnum.PUBLISHED);
