import {defineStore} from "pinia";
import {ref} from "vue";

import {removeDraftPost} from "@/entities/post";
import type {PostDraft, ICorePost} from "@/entities/post";

import {useRequest} from "@/shared/api";
import {DEFAULT_ERROR_MESSAGE} from "@/shared/config";

import * as api from "../api/draft-api";
import {getDraftPost} from "../api/post-api";
import {createPost} from "../lib/create-post";
import {isEmpty} from "../lib/is-post-content-empty";


export const useDraftStore = defineStore('draft-store', () => {
	const corePost = ref<ICorePost>(createPost());
	const post = ref<PostDraft | null>(null);
	const {request, isLoading, error} = useRequest(DEFAULT_ERROR_MESSAGE);

	const setCorePost = (newPost: ICorePost) => {
		corePost.value = newPost;
	}

	const setPost = (newPost: PostDraft | null) => {
		post.value = newPost;

		if (newPost) {
			setCorePost(newPost);
		}
	}

	const load = async (slug: string) => {
		setPost(await request(() => getDraftPost(slug)));
	};

	const save = async () => {
		if (!corePost.value) {
			error.value = {
				message: 'Отсутствует черновик'
			};
			return;
		}

		const lastCorePost = corePost.value;
		const {title, content, tags} = lastCorePost;

		if (!title) {
			error.value = {
				message: 'Укажите заголовок поста'
			};
			return;
		}

		if (isEmpty(content)) {
			error.value = {
				message: 'Пост пуст!'
			};
			return;
		}

		if (tags.length < 2) {
			error.value = {
				message: 'Укажите минимум 2 тега'
			};
			return;
		}

		setPost(await request<PostDraft>(() => api.savePost(lastCorePost)));
	};

	const publish = async () => {
		const published = post.value;

		if (!published) {
			error.value = {
				message: 'Не загружен пост'
			};
			return;
		}

		await request(() => api.publishPost(published.slug));

		if (!error.value) {
			setPost(null);
			setCorePost(createPost());
		}
	};

	const remove = async () => {
		const slug = post.value?.slug;

		if (!slug) {
			error.value = {
				message: 'Отсутствует черновик'
			};
			return;
		}

		await request(() => removeDraftPost(slug));
	}

	const reset = () => {
		setPost(null);
		setCorePost(createPost());
	}

	return {
		post, corePost, isLoading, error,
		setPost, setCorePost, load, save, publish, remove, reset
	};
});
