// eslint-disable-next-line
import type {PostVoteValue} from '@/features/change-post-rating';

// eslint-disable-next-line
import type {ShortUserProfile} from '@/entities/user';

export enum PostStatusEnum {
    DRAFT = 'draft',
    PUBLISHED = 'published',
    DELETED = 'deleted'
}

export enum PostContentTypes {
    TEXT = 'text',
    HTML = 'html',
    IMAGE = 'image',
}

export type TextContentBlock = {
    type: PostContentTypes.TEXT,
    value: string,
}
export type HTMLContentBlock = {
    type: PostContentTypes.HTML,
    value: string,
}
export type ImageContentBlock = {
    type: PostContentTypes.IMAGE,
    width: number,
    height: number,
    src: string,
    value?: string, //TODO temp
}
export type PostContentBlock = TextContentBlock | HTMLContentBlock | ImageContentBlock;
export type PostContent = PostContentBlock[];

export interface ICorePost {
    title: string | null;
    content: PostContent;
    tags: string[];
}
export interface IPost extends ICorePost {
    uuid: string,
    author: ShortUserProfile
    title: string,
    slug: string,
    content: PostContent,
    tags: string[],
    post_group: number;
    community: number;
    status: PostStatusEnum;
}
export interface PostDraft extends IPost {
    status: PostStatusEnum.DRAFT
}
export interface PostPublished extends IPost {
    views_count: number;
    comments_count: number;
    votes_up_count: number;
    votes_down_count: number;
    rating: number;
	status: PostStatusEnum.PUBLISHED;
	time_ago: string;
    published_at: Date;
    description: string;
    voted: PostVoteValue;
}
