import {defineStore} from "pinia";
import {ref} from "vue";

import type {PostPublished} from "@/entities/post";
import {getPublishedPost} from "@/entities/post";

import {useRequest} from "@/shared/api";
import {DEFAULT_ERROR_MESSAGE} from "@/shared/config";

export const usePostStore = defineStore('post-store', () => {
	const { request, isLoading, error } = useRequest(DEFAULT_ERROR_MESSAGE);
	const post = ref<PostPublished | null>(null);

	const setPost = (newPost: PostPublished | null) => {
		post.value = newPost;
	};

	const load = async (slug: string) => {
		setPost(await request<PostPublished>(() => getPublishedPost(slug)));
	};

	return {
		post,
		isLoading, error,
		setPost, load
	};
});
