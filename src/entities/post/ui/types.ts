import type {PostPublished, PostDraft} from '@/entities/post';

export interface PreviewPostProps {
	post: PostPublished | PostDraft;
}
