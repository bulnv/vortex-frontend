import type {PostPublished, PostDraft} from "@/entities/post";

import {getAPI} from "@/shared/api";
import {APINames} from "@/shared/config";


export const getPost = async <Post> (slug: string) =>
    getAPI(APINames.MONOLITH).getWithCredentials<Post>(`/posts/${slug}/`);

export const getPublishedPost = async (slug: string): Promise<PostPublished> => {
    return await getPost<PostPublished>(slug);
};

export const getDraftPost = async (slug: string): Promise<PostDraft> => {
    return await getPost<PostDraft>(slug);
};

export const removeDraftPost = async (slug: string) => {
	await getAPI(APINames.MONOLITH).remove(`/posts/${slug}/`);
};
