import { ref } from "vue";

export const usePostCollapse = () => {
	const MAX_POST_CONTENT_HEIGHT = 500;
	const MIN_POST_HIDDEN_CONTENT_HEIGHT = 500;
	const collapsable = ref(false);
	const collapsed = ref(false);

	const toggleCollapsed = (postElement: HTMLElement) => {
		collapsed.value = !collapsed.value;

		if (collapsed.value) {
			postElement.scrollIntoView({
				behavior: "instant",
				block: "nearest",
				inline: "nearest"
			});
		}
	}

	const setup = (postElement: HTMLElement) => {
		const postContentHeight = postElement.offsetHeight;
		const postHiddenContentHeight = postContentHeight - MAX_POST_CONTENT_HEIGHT;
		const isCollapsable =
			postContentHeight > MAX_POST_CONTENT_HEIGHT &&
			postHiddenContentHeight > MIN_POST_HIDDEN_CONTENT_HEIGHT;

		collapsable.value = isCollapsable;
		collapsed.value = isCollapsable;
	};

	return {
		collapsed, collapsable,
		toggleCollapsed, setup,
	};
}
