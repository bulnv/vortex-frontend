import {PostContentTypes} from "@/entities/post";
import type {PostContentBlock} from "@/entities/post";

export const createImageBlock = (): PostContentBlock => {
    return {
        type: PostContentTypes.IMAGE,
        width: 0,
        height: 0,
        src: '',
    };
};

export const createHTMLBlock = (): PostContentBlock => {
    return {
        type: PostContentTypes.HTML,
        value: '',
    };
};
