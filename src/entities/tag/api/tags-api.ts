import {getAPI} from "@/shared/api";
import {APINames} from "@/shared/config";

import type {FindTagResponse} from "./types";

export const findTag = async (tag: string): Promise<FindTagResponse> => {
	return await getAPI(APINames.MONOLITH).getWithCredentials<FindTagResponse>('/tags/', {
		s: tag
	});
}
