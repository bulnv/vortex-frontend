import {getAPI} from "@/shared/api";
import {APINames} from "@/shared/config";
import {getTimeAgo} from "@/shared/lib/time-tools";

import type {Comment, CommentContent} from '../types';

export const saveComment = async (slug: string, content: CommentContent) => {
	const comment = await getAPI(APINames.MONOLITH).post<Comment>('/comments/', {
		post: slug,
		content,
	});

	return {
		...comment,
		children: comment.children || [],
		rating: comment.rating || 0,
	};
};

export const saveReply = async (slug: string, parentUUID: string, content: CommentContent) => {
	const comment = await getAPI(APINames.MONOLITH).post<Comment>('/comments/', {
		post: slug,
		parent: parentUUID,
		content,
	});

	return {
		...comment,
		children: comment.children || [],
		rating: comment.rating || 0,
	};
};

export const getPostComments = async (slug: string) => {
	const comments = await getAPI(APINames.MONOLITH).get<Comment[]>('/comments/', {
		post: slug
	});

	return comments.map(comment => {
		const createdAtDate = new Date(comment.created_at);
		return {
			...comment,
			created_at: createdAtDate,
			time_ago: getTimeAgo(createdAtDate),
		}
	});
};

export const getChildrenComments = async (slug: string, parentUUID: string) => {
	const comments = await getAPI(APINames.MONOLITH).get<Comment[]>('/comments/', {
		post: slug,
		parent: parentUUID,
	});

	return comments.map(comment => {
		const createdAtDate = new Date(comment.created_at);
		return {
			...comment,
			created_at: createdAtDate,
			time_ago: getTimeAgo(createdAtDate),
		}
	});
};
