import {ref, reactive} from 'vue';

import {useRequest} from "@/shared/api";
import {DEFAULT_ERROR_MESSAGE} from "@/shared/config";

import {saveReply, saveComment} from "../api/comment-api";
import {createTextItem} from "../lib/create-item";
import type {Comment, CommentContent,ImageContentBlock, HTMLContentBlock} from "../types";



export const useCommentDraftController = () => {
	const {request, isLoading, error} = useRequest(DEFAULT_ERROR_MESSAGE);

	const comment = ref<Comment | null>(null);
	const text = reactive<HTMLContentBlock>(createTextItem(''));
	const images = ref<ImageContentBlock[]>([]);

	const setText = (newText: string) => {
		text.value = newText;
	};

	const setImages = (newImages: ImageContentBlock[]) => {
		images.value = newImages;
	};

	const addImages = (newImage: ImageContentBlock) => {
		images.value.push(newImage);
	};

	const removeImage = (index: number) => {
		images.value = images.value.filter((_: ImageContentBlock, idx: number) => idx !== index);
	};

	const reset = () => {
		setText('');
		setImages([]);
	}

	const save = async (slug: string, parentUUID?: string): Promise<Comment | null> => {
		if (!text.value && images.value.length === 0) {
			error.value.message = 'Комментарий пуст';
			return null;
		}

		let newComment: Comment | null;
		const content: CommentContent = [
			text, ...images.value
		];

		if (parentUUID) {
			newComment = await request(() => saveReply(slug, parentUUID, content));
		} else {
			newComment = await request(() => saveComment(slug, content));
		}

		if (comment.value) {
			reset();
		}

		return newComment;
	};

	return {
		text, images, isLoading, error,
		setText,
		setImages, removeImage, addImages,
		save, reset
	};
};
