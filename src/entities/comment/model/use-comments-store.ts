
import {defineStore} from "pinia";
import {ref} from "vue";

import {useRequest} from "@/shared/api";
import {DEFAULT_ERROR_MESSAGE} from "@/shared/config";

import {getPostComments} from "../api/comment-api";
import type {Comment} from "../types";


export const useCommentsStore = defineStore('comment-store', () => {
	const { request, isLoading, error } = useRequest(DEFAULT_ERROR_MESSAGE);
	const slug = ref<string | null>(null);
	const comments = ref<Comment[] | null>([]);

    const setSlug = (newSlug: string | null) => {
        slug.value = newSlug;
    };

	const setComments = (newComments: Comment[] | null) => {
		comments.value = newComments;
	};

	const addComment = (comment: Comment, parent?: Comment) => {
		if (!comments.value)
			return;

		if (parent) {
			parent.children.push(comment);
			return;
		}

		comments.value.push(comment);
	}

	const load = async (newSlug: string) => {
		setSlug(newSlug);
		setComments(await request<Comment[]>(() => getPostComments(newSlug)));
	};

	return {
		comments, slug,
		isLoading, error,
        setSlug, setComments, addComment, load
	};
});
