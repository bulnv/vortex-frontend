//TODO
// eslint-disable-next-line
import type {ShortUserProfile} from "@/entities/user";

export enum CommentContentTypes {
	HTML = 'html',
	IMAGE = 'image',
}

export type HTMLContentBlock = {
	type: CommentContentTypes.HTML,
	value: string,
}
export type ImageContentBlock = {
	type: CommentContentTypes.IMAGE,
	width: number,
	height: number,
	src: string,
}
export type CommentContentBlock = HTMLContentBlock | ImageContentBlock;
export type CommentContent = CommentContentBlock[];

export type Comment = {
	uuid: string,
	author: ShortUserProfile
	content: CommentContent,
	rating: number,
	votes_up_count: number,
	votes_down_count: number,
	voted: number | null,
	created_at: Date,
	time_ago: string,
	level: number,
	children: Comment[],
	can_edit: boolean
};
