import {CommentContentTypes} from '../types';
import type {HTMLContentBlock, ImageContentBlock} from '../types';

export const createTextItem = (text: string): HTMLContentBlock => {
	return {
		type: CommentContentTypes.HTML,
		value: text,
	};
}

export const createImageItem = (src: string, width: number, height: number): ImageContentBlock => {
	return {
		type: CommentContentTypes.IMAGE,
		src, width, height,
	};
}
