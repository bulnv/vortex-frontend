import {watchEffect} from "vue";
import type {Router} from "vue-router";

import {useViewerStore, AuthStatues} from "@/entities/user";

import {Routes} from "@/shared/lib/navigation";

export const useRedirectUnauthorized = (router: Router) => {
	const viewer = useViewerStore();

	watchEffect(() => {
		if (viewer.authStatus === AuthStatues.NOT_AUTHORIZED) {
			router.push({ name: Routes.TRENDS });
		}
	});
};
