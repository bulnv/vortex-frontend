import {defineStore} from 'pinia';
import {ref} from 'vue';

import {useRequest} from "@/shared/api";
import {DEFAULT_ERROR_MESSAGE} from "@/shared/config";

import type {
	LoginResponse,
	PasswordConfirmResponse,
	RegisterResponse,
	ConfirmCodeResponse, RequestCodeResponse
} from "../../../user/api/types";
import {RequestCodeTypes} from "../../../user/api/types";
import * as api from "../../api/auth-api";

//TODO
// eslint-disable-next-line
import * as sessionsLocalStorage from '@/app/providers/api/model/session-local-storage';

type Passport = {
	email: string | null,
	username: string | null,
}

export const useAuthStore = defineStore('auth-store', () => {
	const { isLoading, error, request } = useRequest(DEFAULT_ERROR_MESSAGE);
	const passport = ref<Passport | null>(null);

	const setAuthTokens = (refresh_token: string | null, access_token: string | null) => {
		sessionsLocalStorage.setRefreshToken(refresh_token);
		sessionsLocalStorage.setAccessToken(access_token);
	}

	const setPassport = (email: string, username: string | null) => {
		passport.value = { email, username };
	}

	const login = async (username: string, password: string) => {
		const response = await request<LoginResponse>(() => api.login(username, password));

		if (response) {
			setAuthTokens(response.refresh_token, response.access_token);
			setPassport(response.email, response.username);
		}
	};

	const logout = async () => {
		//TODO work with sessions
		setAuthTokens(null, null);
	};


	const resetPassword = async (email: string) => {
		await request(() => api.passwordReset(null, email));
	};

	const confirmPassword = async (code: string, password: string) => {
		await request<PasswordConfirmResponse>(() => api.passwordConfirm(code, password));
	};

	const register = async (email: string, username: string, password: string) => {
		const response = await request<RegisterResponse>(() => api.register(email, username, password));

		if (response) {
			setAuthTokens(response.refresh_token, response.access_token);
			setPassport(response.email, response.username);
		}
	};

	const confirmCode = async (code: string) => {
		return await request<ConfirmCodeResponse>(() => api.confirmCode(code));
	}

	const requestCode = async (type: RequestCodeTypes) => {
		return await request<RequestCodeResponse>(() => api.requestCode(type));
	}

	return {
		passport, isLoading, error,
		setPassport,
		login, logout, resetPassword, confirmPassword,
		register, confirmCode, requestCode
	};
});
