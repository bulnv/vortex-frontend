import {defineStore} from 'pinia';
import {ref} from 'vue';

import {useRequest} from "@/shared/api";
import {DEFAULT_ERROR_MESSAGE} from "@/shared/config";

import { getUser } from '../../api/viewer-api';
import { LOCAL_STORAGE_USERNAME } from '../../config';
import {AuthStatues} from "../../model/types";
import type { UserProfile } from '../../model/types';

export const useViewerStore = defineStore('viewer-store', () => {
	const {request, isLoading, error} = useRequest(DEFAULT_ERROR_MESSAGE);
	const profile = ref<UserProfile | null>(null);
	const authStatus = ref<AuthStatues>(AuthStatues.NOT_LOADED);

	const setAuthStatus = (status: AuthStatues) => {
		authStatus.value = status;
	};

	const setViewer = (newProfile: UserProfile | null) => {
		if (newProfile) {
			setAuthStatus(AuthStatues.AUTHORIZED);
			profile.value = newProfile;
			localStorage.setItem(LOCAL_STORAGE_USERNAME, profile.value.username);
		} else {
			setAuthStatus(AuthStatues.NOT_AUTHORIZED);
			profile.value = null;
			localStorage.removeItem(LOCAL_STORAGE_USERNAME);
		}
	};

	const load = async (username: string) => {
		setAuthStatus(AuthStatues.PENDING);
		const profile = await request<UserProfile>(() => getUser(username));
		setViewer(profile);
	};

	const savedUsername = localStorage.getItem(LOCAL_STORAGE_USERNAME);

	if (savedUsername) {
		load(savedUsername);
	} else {
		setAuthStatus(AuthStatues.NOT_AUTHORIZED);
	}

	return {
		profile, authStatus,
		isLoading, error,
		load, setViewer,
	};
});
