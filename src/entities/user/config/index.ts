export const LOCAL_STORAGE_SESSION_UUID_KEY = 'sessionUUID';
export const LOCAL_STORAGE_ACCESS_KEY = 'access';
export const LOCAL_STORAGE_REFRESH_KEY = 'refresh';
export const LOCAL_STORAGE_USERNAME = 'username';
export const LOCAL_STORAGE_SETTINGS_KEY = 'user-settings';
