export type RegisterResponse = {
    email: string,
    uuid: string,
    username: string,
    access_token: string,
    refresh_token: string
}

export type LoginResponse = {
    email: string,
    uuid: string,
    username: string,
    access_token: string,
    refresh_token: string
}

export type RefreshResponse = {
    access_token: string,
    refresh_token: string
}

export type GetConfirmCodeResponse = {
    type: string,
    code: string,
    url: string,
}

export type ConfirmCodeResponse = {
    email: string,
    uuid: string,
    username: string,
    access_token: string,
    refresh_token: string
}

export enum RequestCodeTypes {
	EMAIL = 'email',
	TELEGRAM = 'telegram'
}

export type RequestCodeResponse = {
	type: string,
	code: string,
	url: string
}

export type PasswordConfirmResponse = {
    email: string,
    uuid: string,
    username: string,
    access_token: string,
    refresh_token: string
}

export type GetAllSessionsResponse = {
    uuid: string,
    ip: string,
    useragent: string,
    last_activity: string,
    created_at: string
}[];
