import {getAPI} from "@/shared/api";
import {APINames} from "@/shared/config";

import { RequestCodeTypes } from "../api/types";
import type * as APITypes from '../api/types';

export const register = async (email: string, username: string, password: string) => {
	return await getAPI(APINames.AUTH).post<APITypes.RegisterResponse>('/user/register/', {
		email,
		username,
		password
	});
};

export const login = async (username: string, password: string) => {
	return await getAPI(APINames.AUTH).post<APITypes.LoginResponse>('/user/login/', {
		username, password
	});
};

export const refreshToken = async (refresh_token: string) => {
	return await getAPI(APINames.AUTH).post<APITypes.RefreshResponse>('/token/refresh/', {
		refresh_token
	});
};

export const getCode = async (code: string) => {
	return await getAPI(APINames.AUTH).get<APITypes.GetConfirmCodeResponse>(`/code/${code}`);
};

export const confirmCode = async (code: string) => {
	return await getAPI(APINames.AUTH).post<APITypes.ConfirmCodeResponse>('/code/confirm/', {
		code
	});
};

export const requestCode = async (type: RequestCodeTypes) => {
	return await getAPI(APINames.AUTH).post<APITypes.RequestCodeResponse>('/code/', {
		type
	});
};

export const passwordReset = async (username: string | null, email: string | null) => {
	if (username == null && email == null) {
		throw new Error('Нужен хотя-бы один из двух параметров: email, username');
	}

	return await getAPI(APINames.AUTH).post('/password/reset/', {
		username, email
	});
};

export const passwordConfirm = async (code: string, password: string) => {
	return await getAPI(APINames.AUTH).post<APITypes.PasswordConfirmResponse>('/password/confirm/', {
		code, password
	});
};

export const getAllSessions = async () => {
	return await getAPI(APINames.AUTH).get<APITypes.GetAllSessionsResponse>('/session/');
};

export const removeAllSessions = async (except_current: boolean) => {
	await getAPI(APINames.AUTH).remove('/session/', {
		except_current
	});
};

export const removeSession = async (uuid: string) => {
	await getAPI(APINames.AUTH).remove(`/session/${uuid}`);
};
