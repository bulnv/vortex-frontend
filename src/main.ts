import {useNavigation } from '@/shared/providers/stores';

import {createApp, registerPlugins,} from './app';
import App from './app/App.vue';
import {routes} from './pages';

/**
 * SPA entry point
 */
const {app, router} = createApp({App, routes});

const { setRoutes } = useNavigation();
setRoutes(routes);

router
	.isReady()
	.then(() => {
		registerPlugins(app);
		app.mount('#app');
	});
