import {SsrRenderer} from '../.builder';

import {createApp} from './app';
import App from './app/AppSsg.vue';
import {routes} from './pages/ssg';

/**
 * SSG server entry point
 */
export async function render(route: string, manifest: Record<string, string[]>) {
	const instance = createApp({App, routes});

	return await SsrRenderer.renderApp({
		route,
		manifest,
		instance,
	});
}
