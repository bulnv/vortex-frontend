// eslint-disable-next-line
import type {RouteRecordRaw} from 'vue-router';
// eslint-disable-next-line
import {meta} from '@/app/providers/router';

import {Routes} from '@/shared/lib/navigation';

//TODO Temp
const notFound = () => import('@/pages/not-found/NotFoundPage.vue');

export const routes: Record<string, RouteRecordRaw> = {
	[Routes.TRENDS]: {
		path: '/trends',
		component: () => import('@/pages/posts-feeds/TrendsFeedPage.vue'),
		meta: meta('Тренды'),
	},
	[Routes.NEW]: {
		path: '/',
		alias: '/new',
		component: () => import('@/pages/posts-feeds/NewFeedPage.vue'),
		meta: meta('Новое'),
	},
	[Routes.TOP]: {
		path: '/top',
		component: () => import('@/pages/posts-feeds/TopFeedPage.vue'),
		meta: meta('Топ'),
	},
	[Routes.DISCUSSED]: {
		path: '/discussed',
		component: () => import('@/pages/posts-feeds/DiscussedFeedPage.vue'),
		meta: meta('Обсуждаемое'),
	},
	[Routes.SUBSCRIPTIONS]: {
		path: '/subscriptions',
		component: () => import('@/pages/posts-feeds/SubscriptionsFeedPage.vue'),
		meta: meta('Подписки'),
	},
	[Routes.COPYRIGHT]: {
		path: '/copyright',
		component: () => import('@/pages/posts-feeds/CopyrightFeedPage.vue'),
		meta: meta('Авторское'),
	},
	[Routes.GROUPS]: {
		path: '/groups',
		component: () => import('@/pages/posts-feeds/GroupsFeedPage.vue'),
		meta: meta('Группы'),
	},
	[Routes.GROUP_CREATE]: {
		path: '/group_create',
		component: () => import('@/pages/group-create/GroupCreatePage.vue'),
		meta: meta('Создание группы'),
	},
	[Routes.TAGS]: {
		path: '/tags',
		component: () => import('@/pages/posts-feeds/TagsFeedPage.vue'),
		meta: meta('Теги'),
	},
	[Routes.POST_EDITOR]: {
		path: '/post-editor/:slug?',
		alias: '/post-editor',
		component: () => import('@/pages/post-editor/PostEditorPage.vue'),
		meta: meta('Создать пост'),
	},
	[Routes.POST_PUBLISHED]: {
		path: '/publish/:slug',
		component: () => import('@/pages/post-published/PostPublishedPage.vue'),
		meta: meta('Пост опубликован'),
	},
	[Routes.POST_PREVIEW]: {
		path: '/post-preview/:slug',
		component: () => import('@/pages/post-preview/PostPreviewPage.vue'),
		meta: meta('Предпросмотр поста'),
	},
	[Routes.POST]: {
		path: '/post/:slug',
		component: () => import('@/pages/post-page/PostPage.vue'),
		meta: meta('Пост'),
	},
	[Routes.PROFILE]: {
		path: '/profile/:username?',
		alias: '/profile',
		name: Routes.PROFILE,
		component: () => import('@/pages/profile-page/ProfilePage.vue'),
		meta: meta('Профиль'),
	},
	[Routes.SUPPORT]: {
		path: '/support',
		component: notFound,
		meta: meta('Помощь и поддержка'),
	},
	[Routes.ABOUT]: {
		path: '/about',
		component: notFound,
		meta: meta('О проекте'),
	},
	[Routes.NEWS]: {
		path: '/news',
		component: notFound,
		meta: meta('Новости проекта'),
	},
	[Routes.RULES]: {
		path: '/rules',
		component: notFound,
		meta: meta('Правила сообщества'),
	},
	[Routes.CONTACTS]: {
		path: '/contacts',
		component: notFound,
		meta: meta('Контакты'),
	},
	[Routes.REPLIES]: {
		path: '/replies',
		component: notFound,
		meta: meta('Ответы'),
	},
	[Routes.COMMENTS]: {
		path: '/comments',
		component: notFound,
		meta: meta('Комментарии'),
	},
	[Routes.SAVED]: {
		path: '/saved',
		component: notFound,
		meta: meta('Сохранённое'),
	},
	[Routes.VIEWED]: {
		path: '/viewed',
		component: notFound,
		meta: meta('Просмотренное'),
	},
	[Routes.BLACK_LIST]: {
		path: '/black-list',
		component: notFound,
		meta: meta('Чёрный список'),
	},
	[Routes.SETTINGS]: {
		path: '/settings',
		component: notFound,
		meta: meta('Настройки'),
	},
	[Routes.LOGOUT]: {
		path: '/logout',
		component: notFound,
		meta: meta('Выход'),
	},
	[Routes.PRE_MODERATION_POSTS]: {
		path: '/pre-moderation-posts',
		component: notFound,
		meta: meta('Посты на премодерации'),
	},
	[Routes.GROUP_REPLIES]: {
		path: '/group-replies',
		component: notFound,
		meta: meta('Ответы'),
	},
	[Routes.GROUP_MODERATORS]: {
		path: '/group-moderators',
		component: notFound,
		meta: meta('Модераторы'),
	},
	[Routes.GROUP_BLACK_LIST]: {
		path: '/group-black-list',
		component: notFound,
		meta: meta('Черный список'),
	},
	[Routes.GROUP_SETTINGS]: {
		path: '/group-settings',
		component: notFound,
		meta: meta('Настройки'),
	},
	[Routes.MODERATION_CALLS]: {
		path: '/moderation-calls',
		component: notFound,
		meta: meta('Вызов модератора'),
	},
	[Routes.USERS_BANNED]: {
		path: '/users-banned',
		component: notFound,
		meta: meta('Забаненные пользователи'),
	},
	[Routes.STATISTICS]: {
		path: '/stats',
		component: notFound,
		meta: meta('Статистика'),
	},
	[Routes.MODERATORS_LIST]: {
		path: '/moderators-list',
		component: notFound,
		meta: meta('Список модераторов'),
	},
	[Routes.USER_AGREEMENT]: {
		path: '/user-agreement',
		component: notFound,
		meta: meta('Пользовательское соглашение'),
	},
	[Routes.DRAFTS]: {
		path: '/drafts',
		component: () => import('@/pages/post-draft/PostDraftPage.vue'),
		meta: meta('Черновики'),
	},
	[Routes.NOT_FOUND]: {
		path: '/:pathMatch(.*)*',
		component: notFound,
		meta: meta('Не найдено'),
	},
	[Routes.ACCOUNT_VERIFICATION]: {
		path: '/activate',
		component: () => import('@/pages/account-verification/AccountVerification.vue'),
		meta: meta('Подтверждение почты'),
	},
};
