//TODO
// eslint-disable-next-line
import {meta} from '@/app/providers/router';
// eslint-disable-next-line
import type {RouteRecordRaw} from 'vue-router';

/**
 * Enums for navigation
 */
export enum Routes {
	MAIN = 'MAIN',
	FORBIDDEN = 'FORBIDDEN',
	NOT_FOUND = 'NOT_FOUND',
	SERVER_ERROR = 'SERVER_ERROR',
}

/**
 * Route map for router
 */
export const routes: Record<string, RouteRecordRaw> = {
	[Routes.MAIN]: {
		path: '/',
		component: () => import('@/pages/ssg/MainPage.vue'),
		meta: meta('Главная'),
	},
	[Routes.FORBIDDEN]: {
		path: '/403',
		component: () => import('@/pages/ssg/403Page.vue'),
		meta: meta('Доступ запрещён'),
	},
	[Routes.NOT_FOUND]: {
		path: '/404',
		component: () => import('@/pages/ssg/404Page.vue'),
		meta: meta('Не найдено'),
	},
	[Routes.SERVER_ERROR]: {
		path: '/500',
		component: () => import('@/pages/ssg/500Page.vue'),
		meta: meta('Ошибка сервера'),
	},
};
