import type {Comment} from '@/entities/comment';

export type NotAuthorizedModalEmits = {
    (e: 'close-modal'): void;
};

export type PostRatingDetailsProps = {
	post: Comment;
};

export type PostRatingDetailsEmits = {
	(e: 'close'): void;
};
