import type {Comment} from "@/entities/comment";

export type ChangePostRatingProps = {
	post: Comment;
};

export type ChangePostRatingEmits = {
	(e: 'show-details'): void;
};

export type PostRatingProps = {
	comment: Comment;
};
