import CommentRating from './CommentRating.vue';

export type {PostVoteValue} from './api/types';

export { CommentRating };
