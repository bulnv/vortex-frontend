import { defineStore } from 'pinia'
import { ref } from 'vue'

import { authApi, RequestCodeTypes } from '@/entities/user';

// todo real verification
export const useVerificationStore = defineStore('verification-store', () => {
	const loading = ref(false);
	const error = ref(false);

	const confirmVerification = async () => {
		loading.value = true;
		error.value = false;
		const response = await authApi.requestCode(RequestCodeTypes.TELEGRAM);
		const { url } = response;
		loading.value = false;

		if (url) {
			window.location.href = url;
		} else {
			error.value = true;
		}
	}

	return { loading, error, confirmVerification };
});

