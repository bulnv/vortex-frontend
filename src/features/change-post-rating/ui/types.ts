import type { PostPublished } from "@/entities/post";

export type NotAuthorizedModalEmits = {
    (e: 'close-modal'): void;
};

export type PostRatingDetailsProps = {
	post: PostPublished;
};

export type PostRatingDetailsEmits = {
	(e: 'close'): void;
};
