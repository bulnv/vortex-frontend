import {getAPI} from "@/shared/api";
import {APINames} from "@/shared/config";

import type { PostVoteValue } from './types';

export const changePostRating = async (slug: string, value: PostVoteValue) => {
	await getAPI(APINames.MONOLITH).post(`/posts/${slug}/vote/`, {
		value
	});
};
