import { ref } from 'vue';

import { PostVoteValue } from '../api/types';

export const useRatingHandlers = (initialRating: number, voted: number | null) => {
	const rating = ref<number>(initialRating);

	const ratingDecreased = ref(Boolean(voted == PostVoteValue.MINUS));
	const ratingIncreased = ref(Boolean(voted == PostVoteValue.PLUS));

	const handleRatingRaiseClick = () => {
		if (ratingIncreased.value) {
			rating.value--;
		} else {
			rating.value++;
		}

		if (ratingDecreased.value) {
			rating.value++;
		}

		ratingIncreased.value = !ratingIncreased.value;
		ratingDecreased.value = false;
	};

	const handleRatingDecreaseClick = () => {
		if (ratingDecreased.value) {
			rating.value++;
		} else {
			rating.value--;
		}

		if (ratingIncreased.value) {
			rating.value--;
		}

		ratingDecreased.value = !ratingDecreased.value;
		ratingIncreased.value = false;
	};

	return {
		rating,
		ratingDecreased,
		ratingIncreased,
		handleRatingRaiseClick,
		handleRatingDecreaseClick
	};
};
