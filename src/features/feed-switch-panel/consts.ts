export const feeds: Record<string, { id: number, title: string }> = {
	popular: {
		id: 0,
		title: 'Популярное',
	},
	new: {
		id: 1,
		title: 'Новое',
	},
	top: {
		id: 2,
		title: 'Топ',
	},
	mostCommented: {
		id: 3,
		title: 'Обсуждаемое',
	}
};
