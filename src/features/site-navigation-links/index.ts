import SiteNavigationLinkMobile from './ui/SiteNavigationLinkMobile.vue';
import SiteNavigationLinks from './ui/SiteNavigationLinks.vue';

export {SiteNavigationLinks, SiteNavigationLinkMobile};
