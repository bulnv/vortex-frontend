import {routes} from "@/pages";

export const links = [
	{...routes.TRENDS, disabled: true},
	{...routes.NEW, disabled: false},
	{...routes.TOP, disabled: false},
	{...routes.DISCUSSED, disabled: false},
	{...routes.SUBSCRIPTIONS, disabled: true},
	{...routes.GROUPS, disabled: true},
	{...routes.COPYRIGHT, disabled: true},
	{...routes.TAGS, disabled: true},
];
