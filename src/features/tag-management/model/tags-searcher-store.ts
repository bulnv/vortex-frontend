import {defineStore} from "pinia";
import {ref} from "vue";

// eslint-disable-next-line
import type {APIError} from "@/app/providers/api";
import type {Tag} from "@/entities/tag";
import { findTag } from '@/entities/tag';


const tagsCache: Record<string, Tag[]> = {};

export const useTagsSearcherStore = defineStore('tags-searcher-store', () => {
    const foundTags = ref<Tag[]>([]);
    const loading = ref<boolean>(false);
    const error = ref<APIError | null>(null);

    const find = async (tag: string) => {
        error.value = null;
        foundTags.value = [];

        if (tagsCache[tag]) {
            foundTags.value = tagsCache[tag];
            return;
        }

        loading.value = true;

        try {
            const response = await findTag(tag);
            foundTags.value = tagsCache[tag] = response.map(item => item.name);
        } catch (err: any) {
            error.value = err;
        } finally {
            loading.value = false;
        }
    };

    return {
        foundTags, loading, error,
        find
    };
});
