export type RegistrationFormEmits = {
    (e: 'submit'): void;
}

export type AccountVerificationFormEmits = {
	(e: 'submit'): void;
};
