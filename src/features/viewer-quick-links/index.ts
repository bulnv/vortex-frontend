import ViewerQuickLinks from './ui/ViewerQuickLinks.vue';
import ViewerQuickLinksMobile from './ui/ViewerQuickLinksMobile.vue';

export {ViewerQuickLinks, ViewerQuickLinksMobile};
