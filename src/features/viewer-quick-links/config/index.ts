import {routes} from "@/pages";

import {Routes} from "@/shared/lib/navigation";

const notification = {
    attract: false,
    count: 0,
};

export const links = {
    REPLIES: {...routes[Routes.REPLIES], name: Routes.REPLIES, title: 'Ответы', notification, disabled: true},
    COMMENTS: {...routes[Routes.COMMENTS], name: Routes.COMMENTS, title: 'Комментарии', notification, disabled: true},
    SAVED: {...routes[Routes.SAVED], name: Routes.SAVED, title: 'Сохранённое', notification, disabled: true},
    VIEWED: {...routes[Routes.VIEWED], name: Routes.VIEWED, title: 'Просмотренное', notification, disabled: true},
    BLACK_LIST: {...routes[Routes.BLACK_LIST], name: Routes.BLACK_LIST, title: 'Игнор-лист', notification, disabled: true},
    SETTINGS: {...routes[Routes.SETTINGS], name: Routes.SETTINGS, title: 'Настройки', notification, disabled: true},
}
