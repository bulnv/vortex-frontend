import { defineStore } from 'pinia';
import { ref } from 'vue';

import { ESidebarPosition } from '../types';

export const useSidebar = defineStore('mobile-sidebar', () => {
	const opened = ref(false);
	const sidebarPosition = ref(ESidebarPosition.LEFT);

	const setPosition = (position: ESidebarPosition) => {
		sidebarPosition.value = position;
	}

	const openSidebar = () => {
		opened.value = true;
	}

	const closeSidebar = () => {
		if (opened.value) {
			opened.value = false;
		}
	}

	const toggleSidebar = () => {
		opened.value = !opened.value;
	}

	const openLeftSidebar = () => {
		openSidebar();
		setPosition(ESidebarPosition.LEFT);
	}

	const openRightSidebar = () => {
		openSidebar();
		setPosition(ESidebarPosition.RIGHT);
	}

	return {
		opened,
		sidebarPosition,
		openSidebar,
		closeSidebar,
		toggleSidebar,
		openLeftSidebar,
		openRightSidebar,
	};
})
