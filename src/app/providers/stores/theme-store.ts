import {defineStore} from 'pinia';
import {ref} from 'vue';

export enum Themes {
	DEFAULT = 'default'
}

export enum ThemeModes {
	AUTO = 'auto',
	LIGHT = 'light',
	DARK = 'dark'
}

const getThemeMode = (html: HTMLElement) => {
	const localStorageTheme = localStorage.getItem('theme');
	let theme = ThemeModes.LIGHT;

	if (localStorageTheme) {
		theme = localStorageTheme as ThemeModes;
	}

	if (html.dataset.colorSchema == ThemeModes.AUTO) {
		const darkThemeMq = window.matchMedia('(prefers-color-scheme: dark)');
		theme = darkThemeMq.matches? ThemeModes.DARK : ThemeModes.LIGHT;
	}

	return theme;
};

/**
 * Theme switcher
 */
export const useThemeStore = defineStore('themeStore', () => {
	const html = document.documentElement;
	const themeMode = ref(getThemeMode(html));

	const setupTheme = () => {
		localStorage.setItem('theme', themeMode.value);
		html.setAttribute('data-color-schema', themeMode.value);
	};

	const setThemeMode = (mode: ThemeModes) => {
		themeMode.value = html.dataset.colorSchema = mode;
		setupTheme();
	};

	setupTheme();

	return {
		themeMode,
		setThemeMode
	};
});
