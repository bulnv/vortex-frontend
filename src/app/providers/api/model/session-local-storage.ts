
import {REFRESH_TOKEN_URL} from "@/shared/config";

import {REFRESH_TOKEN_KEY, ACCESS_TOKEN_KEY} from "../config";
import {createTokenFetcher} from "../lib/create-token-fetcher";

const fetchTokens = createTokenFetcher(REFRESH_TOKEN_URL);

export const getRefreshToken = () => {
	return localStorage.getItem(REFRESH_TOKEN_KEY) || null;
}

export const setRefreshToken = (value: string | null) => {
	if (value)
		return localStorage.setItem(REFRESH_TOKEN_KEY, value);
	return localStorage.removeItem(REFRESH_TOKEN_KEY);
}

export const getAccessToken = () => {
	return localStorage.getItem(ACCESS_TOKEN_KEY) || null;
}

export const setAccessToken = (value: string | null) => {
	if (value)
		return localStorage.setItem(ACCESS_TOKEN_KEY, value);
	return localStorage.removeItem(ACCESS_TOKEN_KEY);
}

export const refreshTokens = async () => {
	const refreshToken = getRefreshToken();
	const accessToken = getAccessToken();

	if (refreshToken && accessToken) {
		const tokens = await fetchTokens(refreshToken, accessToken);

		setAccessToken(tokens.accessToken);
		setRefreshToken(tokens.refreshToken);
	}
};
