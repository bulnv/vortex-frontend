import type {ServiceAPIErrorResponse, APIError} from "../types";

export const getAPIError = (exception: Error | ServiceAPIErrorResponse): APIError => {
	if (exception instanceof Error) {
		return {
			code: null,
			message: exception.message
		};
	}

	if (typeof exception.detail === 'string') {
		return {
			code: exception.code,
			message: exception.detail
		};
	}

	if (exception.detail.length === 0) {
		return {
			code: exception.code,
			message: 'The error does not contain a message'
		};
	}

	if (exception.detail.length > 1) {
		return {
			code: exception.code,
			messages: exception.detail.map((detail) => {
				return detail.msg;
			})
		};
	}

	return {
		code: exception.code,
		message: exception.detail[0].msg
	};
};
