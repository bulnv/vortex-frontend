import Notifications from "@kyvg/vue3-notification";
import {createPinia} from 'pinia';
import {createApp as createSPAApp, createSSRApp, type Component, type App,} from 'vue';
import type {Router, RouteRecordRaw, RouterOptions,} from 'vue-router';

import {createAPI} from "./providers/api";
import {createRouter} from './providers/router';

// Basic shared styles
import '@/shared/ui/styles/index.scss';

/**
 * Shared function between SPA, SSR, SSG
 */
export function createApp({
	App,
	routes,
	hydrate = import.meta.env.VITE_HYDRATE,
	routerOptions,
}: {
	App: Component<unknown>;
	routes: Record<string, RouteRecordRaw>;
	hydrate?: boolean | string;
	routerOptions?: RouterOptions;
}): {
	app: App<Element>;
	router: Router;
} {
	createAPI();

	const store = createPinia();
	const router = createRouter(routes, routerOptions);
	const app = hydrate
		? createSSRApp(App)
		: createSPAApp(App);

	app.use(store);
	app.use(router);
	app.use(Notifications)

	return {app, router};
}
