import { defineStore } from 'pinia';
import { ref } from 'vue';
import type { Ref } from 'vue';
import type { RouteRecordRaw } from 'vue-router';

type Routes = Record<string, RouteRecordRaw>;

export const useNavigation = defineStore('navigation-store', () => {
	const routes = ref<Routes>({}) as Ref<Routes>;

	const setRoutes = (newRoutes: Routes) => {
		routes.value = newRoutes;
	}

	const getRoute = (routeName: string): RouteRecordRaw | undefined => {
		return routes.value[routeName];
	}

	return {
		routes,
		setRoutes,
		getRoute,
	}
})
