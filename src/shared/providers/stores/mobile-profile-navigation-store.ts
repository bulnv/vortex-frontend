import {defineStore} from 'pinia';
import {ref} from 'vue';

export const useMobileProfileNavigationStore = defineStore('mobileProfileNavigationStore', () => {
	const showProfileNavigationPage = ref(false);

	const toggleProfileNavigationPage = () => {
		//TODO close if desktop width
		showProfileNavigationPage.value = !showProfileNavigationPage.value;
	};

	return { showProfileNavigationPage, toggleProfileNavigationPage };
});
