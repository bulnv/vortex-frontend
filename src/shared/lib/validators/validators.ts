import type { ConditionResult, ConditionChecker } from "./types";

export const requiredValidator = (message?: string): ConditionChecker => {
    return (value: string): ConditionResult => {
        return {
            error: !value,
            text: message ? message : 'Обязательное поле',
        };
    };
};

export const emailValidator = (message?: string): ConditionChecker => {
    return (value: string): ConditionResult => {
        return {
            error: /^[a-zA-Z0-9@.'#%&/-]+$/.test(value) && !/^\S+@\S+\.\S+$/.test(value),
            text: message ? message : 'Неверный формат email',
        };
    };
};

export const notLongerThanValidator = (maxLength: number, message?: string): ConditionChecker => {
    return (value: string): ConditionResult => {
        return {
            error: value.length > maxLength,
            text: message ? message : (maxLength === 1 ? `Не более ${maxLength} символа` : `Не более ${maxLength} символов`),
        };
    };
};

export const notShorterThanValidator = (minLength: number, message?: string): ConditionChecker => {
    return (value: string): ConditionResult => {
        return {
            error: value.length < minLength,
            text: message ? message : (minLength === 1 ? `Не менее ${minLength} символа` : `Не менее ${minLength} символов`),
        };
    };
};

export const minNumbersCountValidator = (minCount: number, message?: string): ConditionChecker => {
    return (value: string): ConditionResult => {
        return {
            error: !/[0-9]/.test(value),
            text: message ? message : (minCount === 1 ? `Не менее ${minCount} цифр` : `Не менее ${minCount} цифры`),
        };
    };
};

export const minLetterCountValidator = (minCount: number, message?: string): ConditionChecker => {
    return (value: string): ConditionResult => {
        return {
            error: !/[a-zA-Z]/.test(value),
            text: message ? message : (minCount === 1 ? `Не менее ${minCount} букв` : `Не менее ${minCount} буквы`),
        };
    };
};

export const charactersValidator = (regexp: RegExp, message?: string): ConditionChecker => {
    return (value: string): ConditionResult => {
        return {
            error: value.length === 0 || !regexp.test(value),
            text: message ? message : 'Недопустимые символы'
        };
    };
};

export const valuesMatchValidator = (firstValue: string, message?: string): ConditionChecker => {
    return (secondValue: string): ConditionResult => {
        return {
            error: firstValue != secondValue,
            text: message ? message : 'Значения не совпадают',
        };
    };
}
