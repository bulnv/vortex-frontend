import type { ConditionResult, ConditionChecker } from "./index";

type FieldValue = string | null;

export const createError = (text: string, error?: boolean): ConditionResult => {
	return { error: error === undefined ? true : error, text };
};

export const validate = (value: FieldValue, validators: ConditionChecker[]): ConditionResult[] => {
    const errors: ConditionResult[] = [];

    for (const validator of validators) {
		errors.push(validator(value || ''));
    }

    return errors;
};

export const hasError = (errors: ConditionResult[]) => {
	for (const error of errors) {
		if (error.error) {
			return true;
		}
	}
}

export const hasErrors = (errorsList: (ConditionResult[] | undefined)[]) => {
	for (const errors of errorsList) {
		if (!errors) {
			continue;
		}

		if (hasError(errors)) {
			return true;
		}
	}

	return false;
};
