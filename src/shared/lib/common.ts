export function throttle(fn: Function, wait: number): (...args: any[]) => any {
	let throttled = false;
	return function (this: any, ...args: any[]) {
		if (!throttled) {
			fn.apply(this, args);
			throttled = true;
			setTimeout(() => {
				throttled = false;
			}, wait);
		}
	};
}
