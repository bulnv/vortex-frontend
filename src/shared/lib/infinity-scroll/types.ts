export type InfinityScrollItemProps<T> = {
	item: T,
	index: number,
	isVisible: boolean,
};
export type InfinityScrollProps<T> = {
	list: T[],
	showScrollEnd?: boolean | true,
};

export type InfinityScrollEmits = {
	(e: 'scroll-start'): void;
	(e: 'scroll-end'): void;
};
export type InfinityScrollItemEmits = {
	(e: 'disappeared', index: number): void;
	(e: 'appeared', index: number): void;
};
