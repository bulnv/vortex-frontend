import type {API} from "../types";

let API_MAP: Record<string, API>;

export const setAPI = <T extends string> (newApiList: Record<T, API>) => {
    API_MAP = newApiList;
};

export const getAPI = <T extends string> (name: T) => {
    return API_MAP[name];
};
