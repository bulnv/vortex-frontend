export enum HTTPMethods {
	GET = 'GET',
	POST = 'POST',
	PUT = 'PUT',
	DELETE = 'DELETE',
}

export type Request = <T>(endpoint: string, options?: RequestInit) => Promise<T>;
export type Get = <T>(action: string, params?: Record<string, string>) => Promise<T>;
export type Post = <T>(action: string, params?: Object) => Promise<T>;
export type Put = <T>(action: string, params: Object) => Promise<T>;
export type Delete = <T>(action: string, params?: Object) => Promise<T>;
export type Middleware = <T>(request: () => Promise<T>) => Promise<T>;

export type API = {
	request: Request,
	get: Get,
	getWithCredentials: Get,
	post: Post,
	put: Put,
	remove: Delete
}

export type CreateApi = (config: {
	endpoint: string,
	getAccessToken: () => string | null,
	middleware: Middleware
}) => API;
