import type {Request, Get, Post, Put, Delete, CreateApi} from '@/shared/api/types';

import {HTTPMethods} from '../types';

export const createApi: CreateApi = (config) => {
	const { endpoint } = config;
	const { middleware, getAccessToken } = config;

	const buildOptions = (method: HTTPMethods, params?: Object): RequestInit => {
		const options: RequestInit = {
			method,
			headers: buildHeaders(getAccessToken())
		};

		if (params) {
			options.body = JSON.stringify(params);
		}

		return options;
	};

	const buildHeaders = (accessToken: string | null) => {
		return {
			'Accept': '*/*',
			'Content-Type': 'application/json',
			'Authorization': accessToken ? `Bearer ${accessToken}` : ''
		};
	};

	const buildURL = (action: string, params?: Record<string, string>): string => {
		if (params) {
			action += '?' + new URLSearchParams(params);
		}

		return endpoint + action;
	};

	const request: Request = async (url, options) => {
		const response: Response = await fetch(url, options);
		const text = await response.text();
		let body: Object = {};

		if (text) {
			body = JSON.parse(text);
		}

		if (!response.ok) {
			throw {
				...body,
				code: response.status,
			};
		}

		return body as any;
	};

	const get: Get = async (action, params) => {
		return middleware(() => {
			return request(buildURL(action, params), {
				method: HTTPMethods.GET,
			});
		});
	};

	const getWithCredentials: Get = (action, params) => {
		return middleware(() => {
			return request(buildURL(action, params), buildOptions(HTTPMethods.GET));
		});
	}

	const post: Post = async (action, params) => {
		return middleware(() => {
			return request(buildURL(action), buildOptions(HTTPMethods.POST, params));
		});
	};

	const put: Put = async (action, params) => {
		return middleware(() => {
			return request(buildURL(action), buildOptions(HTTPMethods.PUT, params));
		});
	};

	const remove: Delete = async (action, params) => {
		return middleware(() => {
			return request(buildURL(action), buildOptions(HTTPMethods.DELETE, params));
		});
	};

	return { request, get, getWithCredentials, post, put, remove };
};
