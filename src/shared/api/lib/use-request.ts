import {ref} from "vue";

export const useRequest = (defaultErrorMessage: string) => {
    const isLoading = ref(false);
    const error = ref<any>(null); //todo

    const request = async <Response> (callback: Function): Promise<Response | null> => {
        isLoading.value = true;
        error.value = null;

        try {
            return await callback();
        } catch (err: any) {
			error.value = {
				code: null,
				message: defaultErrorMessage,
				...err
			};

			console.error(error.value.message);
        } finally {
            isLoading.value = false;
        }

		return null;
    };

    return {
        isLoading, error,
        request,
    };
}
