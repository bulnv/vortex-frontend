export enum APINames {
	AUTH = 'auth',
	MONOLITH = 'monolith',
}

export const APIEndpoints: Record<APINames, string> = {
	[APINames.AUTH]: import.meta.env.VITE_AUTH_SERVICE_URL || 'https://auth.kapi.bar/v1',
	[APINames.MONOLITH]: import.meta.env.VITE_MONOLITH_SERVICE_URL || 'https://backend.kapi.bar/v1'
};

export const REFRESH_TOKEN_URL = APIEndpoints[APINames.AUTH] + '/token/refresh/';
