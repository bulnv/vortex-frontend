export type DefaultError = {
    message: string,
    code: null | number
};

export const DEFAULT_ERROR_MESSAGE = `
    Что-то пошло не так. Пожалуйста, обновите страницу.
    Если ошибка повторится, напишите нам в <a href='#'>баг-репорт</a>
`;
