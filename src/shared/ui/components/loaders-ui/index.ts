import BlockLoaderUI from './BlockLoaderUI.vue';
import BlockSuspenseUI from './BlockSuspenseUI.vue';
import FilledLoaderUI from "./FilledLoaderUI.vue";
import LoaderUI from "./LoaderUI.vue";
import SuspenseUI from "./SuspenseUI.vue";

export { BlockLoaderUI, BlockSuspenseUI, LoaderUI, FilledLoaderUI, SuspenseUI };
