export enum IconSizes {
	EIGHT = 'eight',
    TEN = 'ten',
    TWELVE = 'twelve',
    SIXTEEN = 'sixteen',
    EIGHTEEN = 'eighteen',
    TWENTY = 'twenty',
    TWENTY_FOUR = 'twentyFour',
    THIRTY_TWO = 'thirtyTwo',
    THIRTY_SIX = 'thirtySix',
    FORTY = 'forty',
    FORTY_EIGHT = 'fortyEight',
    SIXTY_FOUR = 'sixtyFour',
    NINETY_SIX = 'ninetySix',
}

export type IconProps = {
	size: IconSizes | IconSizes.SIXTEEN
    clickable?: boolean | false,
    disabled?: boolean | false,
};
