import DesktopPageUI from './DesktopPageUI.vue';
import MobilePageUI from './MobilePageUI.vue';

export { DesktopPageUI, MobilePageUI };
