export enum ButtonSize {
	SMALL = 'small'
}

export type Props = {
	size: ButtonSize;
};

export interface IIconButtonProps {
	borderless?: boolean;
	size?: number;
}
