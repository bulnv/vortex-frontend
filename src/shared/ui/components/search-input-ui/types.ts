export interface ISearchInputUI {
	modelValue: string;
	placeholder?: string;
}

export type SearchInputEmits = {
	(e: 'update:modelValue', value: string): void;
	(e: 'enter-end', value: string): void;
	(e: 'search', value: string): void;
}
