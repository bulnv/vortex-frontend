export enum FieldStatuses {
	NONE = '',
	ERROR = 'error',
	SUCCESSFUL = 'successful',
	FOCUS = 'focus'
}

export type FieldValue = string | null;

export type InputEmits = {
	(e: 'update:modelValue', value: FieldValue): void;
};

export type InputConditionResult = {
	error: boolean,
	text: string
}

export type InputProps = {
	label?: string;
	placeholder?: string;
	prefix?: string;
	hideText?: boolean;
	showHint?: boolean;
	modelValue: FieldValue;
	isError?: boolean,
	errors?: InputConditionResult[],
	checkedErrors?: InputConditionResult[],
};


export type TextAreaEmits = {
	(e: 'update:modelValue', value: string | null): void;
	(e: 'removeValue'): void;
};

export type TextAreaValue = string | number | string[] | undefined;

export type TextAreaProps = {
	modelValue: string | null;
};
