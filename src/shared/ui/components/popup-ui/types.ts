export interface IPopupUI {
	open: boolean;
}

export type PopupEmits = {
	(e: 'update:open', value: boolean): void;
}
