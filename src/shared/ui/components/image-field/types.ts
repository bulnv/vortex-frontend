export type ImageDataType = {
    width: number,
    height: number,
    src: string,
}

export type SmallImageFieldEmits = {
    (e: 'imageLoaded', imageData: ImageDataType): void;
};
export type ImageFieldEmits = {
    (e: 'imageRemoved'): void;
    (e: 'imageLoaded', imageData: ImageDataType): void;
};

export type ImageFieldProps = {
    src?: string | null
    openImmediately?: boolean | false
};
