//TODO
// eslint-disable-next-line
import UserIcon from './img/user-picture.svg';

import ArrowDownIcon from './icons/ArrowDownIcon.svg';
import ArrowIcon from './icons/ArrowIcon.svg';
import AttentionIcon from './icons/AttentionIcon.svg';
import Blocked from './icons/Blocked.svg';
import BoldIcon from './icons/BoldIcon.svg';
import BugIcon from './icons/BugIcon.svg';
import CheckIcon from './icons/CheckIcon.svg';
import CloseIcon from './icons/CloseIcon.svg';
import CommentIcon from './icons/CommentIcon.svg';
import ConfigurationIcon from './icons/ConfigurationIcon.svg';
import ErrorCircleIcon from './icons/ErrorCircleIcon.svg';
import ExitIcon from './icons/ExitIcon.svg';
import EyeIcon from './icons/EyeIcon.svg';
import FilterIcon from './icons/FilterIcon.svg';
import HyperlinkIcon from './icons/HyperlinkIcon.svg';
import IgnoreIcon from './icons/IgnoreIcon.svg';
import ImageIcon from './icons/ImageIcon.svg';
import ImageSquareIcon from './icons/ImageSquareIcon.svg';
import ItalicIcon from './icons/ItalicIcon.svg';
import LinkBreakIcon from './icons/LinkBreakIcon.svg';
import ListBulletsIcon from './icons/ListBulletsIcon.svg';
import ListNumbersIcon from './icons/ListNumbersIcon.svg';
import MegaphoneIcon from './icons/MegaphoneIcon.svg';
import MenuIcon from './icons/MenuIcon.svg';
import MinusRoundedOutlineIcon from './icons/MinusRoundedOutlineIcon.svg';
import MoonIcon from './icons/MoonIcon.svg';
import PlusIcon from './icons/PlusIcon.svg';
import PlusRoundedSolidIcon from './icons/PlusRoundedSolidIcon.svg';
import QuoteIcon from './icons/QuoteIcon.svg';
import RateDownIcon from './icons/RateDownIcon.svg';
import RateUpIcon from './icons/RateUpIcon.svg';
import ReloadIcon from './icons/ReloadIcon.svg';
import RespondIcon from './icons/RespondIcon.svg';
import SaveIcon from './icons/SaveIcon.svg';
import SearchIcon from './icons/SearchIcon.svg';
import ShareIcon from './icons/ShareIcon.svg';
import ShortLogoIcon from './icons/ShortLogoIcon.svg';
import SocialTgIcon from './icons/SocialTgIcon.svg';
import SocialVkIcon from './icons/SocialVkIcon.svg';
import StrikeIcon from './icons/StrikeIcon.svg';
import SuccessCircleIcon from './icons/SuccessCircleIcon.svg';
import SunIcon from './icons/SunIcon.svg';
import TinyArrowIcon from './icons/TinyArrowIcon.svg';
import TrashIcon from './icons/TrashIcon.svg';
import WarningCircleIcon from './icons/WarningCircleIcon.svg';
import Capybara403 from './img/Capybara403.png';
import Capybara404 from './img/Capybara404.png';
import Capybara500 from './img/Capybara500.png';
import DarkColorLogo from './img/dark-color-logo.svg';
import Error403Light from './img/Error403.svg';
import Error403Dark from './img/Error403dark.svg';
import Error404Light from './img/Error404.svg';
import Error404Dark from './img/Error404dark.svg';
import Error500Light from './img/Error500.svg';
import Error500Dark from './img/Error500dark.svg';
import WhiteColorLogo from './img/white-color-logo.svg';

export {
	ArrowDownIcon,
	ArrowIcon,
	AttentionIcon,
	BoldIcon,
	BugIcon,
	ConfigurationIcon,
	CommentIcon,
	CloseIcon,
	CheckIcon,
	EyeIcon,
	ExitIcon,
	ErrorCircleIcon,
	HyperlinkIcon,
	IgnoreIcon,
	ItalicIcon,
	ImageIcon,
	ListNumbersIcon,
	ListBulletsIcon,
	MoonIcon,
	MenuIcon,
	MegaphoneIcon,
	PlusIcon,
	QuoteIcon,
	RespondIcon,
	RateUpIcon,
	RateDownIcon,
	SunIcon,
	SearchIcon,
	StrikeIcon,
	ShareIcon,
	SuccessCircleIcon,
	ShortLogoIcon,
	SocialTgIcon,
	SaveIcon,
	SocialVkIcon,
	TrashIcon,
	TinyArrowIcon,
	UserIcon,
	WarningCircleIcon,
	LinkBreakIcon,
	DarkColorLogo,
	WhiteColorLogo,
	ReloadIcon,
	MinusRoundedOutlineIcon,
	PlusRoundedSolidIcon,
	Blocked,
	FilterIcon,
	Error403Light,
	Error403Dark,
	Capybara403,
	Error404Light,
	Error404Dark,
	Capybara404,
	Error500Light,
	Error500Dark,
	Capybara500,
	ImageSquareIcon
};
