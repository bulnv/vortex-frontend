import type { VNodeRef } from 'vue';

import type {Comment} from "@/entities/comment";

export type CommentsTreeNodeRef = { node: Comment; container: VNodeRef; blink: () => void };
