import ConnectorUI from './ConnectorUI.vue';
import HorizontalLineUI from './HorizontalLineUI.vue';
import VerticalLineUI from './VerticalLineUI.vue';
import VerticalSlotLineUI from './VerticalSlotLineUI.vue';

export const LinesUI = {
	Vertical: VerticalLineUI,
	Horizontal: HorizontalLineUI,
	VerticalSlot: VerticalSlotLineUI,
	Connector: ConnectorUI
};
