import { onBeforeUnmount, onMounted, ref, type Ref } from 'vue';

import { throttle } from '@/shared/lib/common';

import { CONSTANTS } from '../config/constants';

export const useCommentsMaxLevel = (): { maxLevel: Ref<number>; ref: Ref<HTMLElement | null> } => {
	const containerRef = ref<HTMLElement | null>(null);
	const resizeObserverRef = ref<ResizeObserver | null>(null);
	const maxLevel = ref(10);

	const limitLevel = (level: number) => {
		if (level < CONSTANTS.MIN_COMMENTS_LEVEL) {
			return CONSTANTS.MIN_COMMENTS_LEVEL;
		}

		if (CONSTANTS.MAX_COMMENTS_LEVEL !== -1 && level > CONSTANTS.MAX_COMMENTS_LEVEL) {
			return CONSTANTS.MAX_COMMENTS_LEVEL;
		}

		return level;
	};

	onMounted(() => {
		const resizeObserver = new ResizeObserver(
			throttle(() => {
				if (containerRef.value) {
					const elementWidth = containerRef.value.clientWidth;

					maxLevel.value = limitLevel(
						Math.floor((elementWidth - CONSTANTS.REQUIRED_COMMENT_WIDTH) / CONSTANTS.LINE_WIDTH)
					);
				}
			}, 300)
		);

		if (containerRef.value) {
			resizeObserver.observe(containerRef.value);
			resizeObserverRef.value = resizeObserver;
		}
	});

	onBeforeUnmount(() => {
		if (resizeObserverRef.value) {
			resizeObserverRef.value.disconnect();
		}
	});

	return { maxLevel, ref: containerRef };
};
