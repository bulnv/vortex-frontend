import { ref } from 'vue';

export const useHover = () => {
	const isHovered = ref(false);

	const props = {
		onmouseover() {
			isHovered.value = true;
		},
		onmouseleave() {
			isHovered.value = false;
		}
	};

	return {
		isHovered,
		props
	};
};
