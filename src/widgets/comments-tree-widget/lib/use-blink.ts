import { ref } from 'vue';

const BLINK_DURATION = 500;
const INTERRUPT_BLINK_DURATION = 150;

export const useBlink = () => {
	const state = ref(false);
	const blinkTimeout = ref<NodeJS.Timeout | null>(null);
	const interruptTimeout = ref<NodeJS.Timeout | null>(null);
	const blink = () => {
		if (state.value) {
			blinkTimeout.value && clearTimeout(blinkTimeout.value);
			state.value = false;

			interruptTimeout.value = setTimeout(() => {
				blink();
			}, INTERRUPT_BLINK_DURATION);
			return;
		}

		interruptTimeout.value && clearTimeout(interruptTimeout.value);
		state.value = true;
		blinkTimeout.value = setTimeout(() => {
			state.value = false;
		}, BLINK_DURATION);
	};

	return {
		blink,
		state
	};
};
