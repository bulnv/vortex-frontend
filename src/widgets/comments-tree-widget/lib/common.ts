export const waitScrollEnd = ({
	timeout: timeoutVal = 2000,
	scrollTimeout: scrollTimeoutVal = 150
}: {
	timeout?: number;
	scrollTimeout?: number;
} = {}): Promise<void> => {
	let scrollHandler: null | (() => void) = null;
	let scrollTimeout: NodeJS.Timeout | null = null;
	let finallyTimeout: NodeJS.Timeout | null = null;
	return new Promise<void>((resolve) => {
		finallyTimeout = setTimeout(() => {
			resolve();
		}, timeoutVal);

		scrollHandler = () => {
			scrollTimeout && clearTimeout(scrollTimeout);
			scrollTimeout = setTimeout(() => {
				resolve();
			}, scrollTimeoutVal);
		};

		scrollHandler();
		window.addEventListener('scroll', scrollHandler);
	}).finally(() => {
		finallyTimeout && clearTimeout(finallyTimeout);
		scrollTimeout && clearTimeout(scrollTimeout);
		window.removeEventListener('scroll', scrollHandler!);
	});
};
