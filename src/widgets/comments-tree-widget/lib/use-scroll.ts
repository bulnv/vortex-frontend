import type { Ref } from 'vue';

import type { CommentsTreeNodeRef } from '../model';

import { waitScrollEnd } from './common';

export const useScroll = (nodeRefs: Ref<CommentsTreeNodeRef[]>) => {
	const scrollToNode = (node: HTMLElement) => {
		const overflow = self.innerHeight < node.offsetHeight;

		node.scrollIntoView({
			block: overflow ? 'start' : 'center',
			behavior: 'smooth'
		});
	};

	const scrollToComment = async (uuid: string) => {
		const index = nodeRefs.value.findIndex((item) => item?.node.uuid === uuid);

		if (index === -1) {
			return;
		}

		const node = nodeRefs.value[index];
		if (node.container) {
			scrollToNode(node.container as unknown as HTMLElement);
			await waitScrollEnd();
			node.blink();
		}
	};

	const scrollToNextComment = async (uuid: string, step: number = 1) => {
		const index = nodeRefs.value.findIndex((item) => item?.node.uuid === uuid);

		if (index === -1 || index + step >= nodeRefs.value.length - 1) {
			return;
		}

		scrollToComment(nodeRefs.value[index + 1].node.uuid);
	};

	const scrollToPreviousComment = async (uuid: string, step: number = 1) => {
		const index = nodeRefs.value.findIndex((item) => item.node.uuid === uuid);

		if (index === -1 || index - step <= 0) {
			return;
		}

		scrollToComment(nodeRefs.value[index - step].node.uuid);
	};

	return {
		scrollToComment: {
			to: scrollToComment,
			next: scrollToNextComment,
			previous: scrollToPreviousComment
		}
	};
};
