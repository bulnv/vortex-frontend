import type {Ref} from "vue";

import type {PostContent, PostContentBlock} from "@/entities/post";

export type ContentController = {
	content: Ref<PostContent>,
	isEmpty: () => boolean,
    getContent: () => PostContent,
    setContent: (newContent: PostContent) => void,
    getBlock: (index: number) => PostContentBlock,
    setBlock: (index: number, data: PostContentBlock) => void,
    addBlock: (block: PostContentBlock) => void,
    removeBlock: (index: number) => void
}
