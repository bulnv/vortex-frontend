export interface IHeaderWidgetProps {
	notFoundLink: string;
	authLink: string;
}
