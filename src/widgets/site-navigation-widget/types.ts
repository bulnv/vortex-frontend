import type {RouteRecordRaw} from "vue-router";

export type INavLink = RouteRecordRaw & {
    title?: string;
    notifications?: number;
    attract?: boolean;
};

//TODO Record<string, string>
export type SidebarNavigationMobileWidgetProps = {
    navigationLinks: INavLink[];
    footerLinks: INavLink[];
    profileLinks?: INavLink[];
    groupLinks?: INavLink[];
    moderationLinks?: INavLink[];
};
