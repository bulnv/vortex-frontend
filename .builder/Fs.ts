import {readFile, rm} from 'node:fs/promises';
import {cwd} from 'node:process';

/**
 * File system additional methods
 */
export class Fs {
	private static _root = cwd();

	static get root(): string {
		return this._root;
	}

	/**
	 * Return file content
	 */
	static async readFile(path: string): Promise<string> {
		return await readFile(path, 'utf8');
	}

	/**
	 * Return parsed JSON file content
	 */
	static async readFileJson<T>(path: string): Promise<T> {
		const file = await readFile(path, 'utf8');

		return JSON.parse(file);
	}

	/**
	 * Remove file or directory
	 */
	static async rm(...paths: string[] | string[][]): Promise<void> {
		const flat = paths.flat();

		for (const path of flat) {
			await rm(path, {recursive: true});
		}
	}
}
