import {basename, extname, parse, join} from 'node:path';
import {readdir, writeFile} from 'node:fs/promises';
import {pathToFileURL} from 'node:url';
import {type App} from 'vue';
import {type Router} from 'vue-router';
import {renderToString, type SSRContext} from 'vue/server-renderer';
import {Fs} from './Fs';

/**
 * SSR helper to transform an application into SSR version
 */
export class SsrRenderer {
	/**
	 * Helper for the server entry point
	 */
	static async renderApp({
		route,
		manifest,
		instance: {app, router},
	}: {
		route: string;
		manifest: Record<string, string[]>;
		instance: {app: App<unknown>, router: Router};
	}): Promise<{html: string, assets: string, ctx: SSRContext}> {
		await router.push(route);
		await router.isReady();

		const ctx: SSRContext = {};
		const html = await renderToString(app, ctx);
		const assets = this.renderAssetLinks(manifest, ctx.modules);

		return {html, assets, ctx};
	}

	/**
	 * Create SSG pages
	 */
	static async transformToSsg({
		entryClient,
		entryServer,
		distPath,
		ssrOutDir,
		routesPath,
		rootPage,
	}: {
		entryClient: string;
		entryServer: string;
		distPath: string;
		ssrOutDir: string;
		routesPath: string;
		rootPage: string;
	}): Promise<void> {
		// Get html template
		const templatePath = join(distPath, parse(entryClient).base);
		const template = await Fs.readFile(templatePath);

		// Get ssr manifest
		const manifestSsrPath = join(distPath, '/ssr-manifest.json');
		const manifestSsr = await Fs.readFileJson<Record<string, string[]>>(manifestSsrPath);

		// Import render function
		const {name} = parse(entryServer);
		const {href} = pathToFileURL(join(ssrOutDir, `/${name}.js`));
		const {render} = await import(href);

		// Create SSG pages
		const routes = await this.getRoutesToTransform(routesPath, rootPage);

		for (const route of routes) {
			const {html, assets} = await render(route, manifestSsr);
			const page = this.replaceMasks(template, {html, assets});
			const distFile = route === '/' ? `/index.html` : `/${route}.html`;

			await writeFile(join(distPath, distFile), page);
		}

		// Remove unused
		await Fs.rm(
			manifestSsrPath,
			templatePath,
			ssrOutDir
		);
	}

	/**
	 * Return html for assets based on manifest
	 */
	private static renderAssetLinks(manifest: Record<string, string[]>, modules: Set<string>): string {
		let links = '';
		const used = new Set();

		for (const id of modules) {
			const files = manifest[id];

			if (!files) {
				continue;
			}

			for (const file of files) {
				if (used.has(file)) {
					continue;
				}

				used.add(file);
				const name = basename(file);

				if (manifest[name]) {
					for (const extraFile of manifest[name]) {
						used.add(extraFile);
						links += this.getAssetLink(extraFile);
					}
				}

				links += this.getAssetLink(file);
			}
		}

		return links;
	}

	/**
	 * Return html tag for asset
	 */
	private static getAssetLink(path: string): string {
		const ext = extname(path);
		const prefix = path[0] === '/' || /^https?/i.test(path) ? '' : '/';

		return {
			'.js': `<link rel="modulepreload" crossorigin href="${prefix}${path}">`,
			'.css': `<link rel="stylesheet" href="${prefix}${path}">`,
			'.woff': `<link rel="preload" href="${prefix}${path}" as="font" type="font/woff" crossorigin>`,
			'.woff2': `<link rel="preload" href="${prefix}${path}" as="font" type="font/woff2" crossorigin>`,
			'.gif': `<link rel="preload" href="${prefix}${path}" as="image" type="image/gif">`,
			'.jpg': `<link rel="preload" href="${prefix}${path}" as="image" type="image/jpeg">`,
			'.jpeg': `<link rel="preload" href="${prefix}${path}" as="image" type="image/jpeg">`,
			'.png': `<link rel="preload" href="${prefix}${path}" as="image" type="image/png">`,
		}[ext] ?? '';
	}

	/**
	 * Return routes for transformation into pages
	 *
	 * TODO: change it, unreliable
	 */
	private static async getRoutesToTransform(path: string, rootPageName: string): Promise<string[]> {
		const pages = await readdir(path);
		const normalizePage = (page: string): string => page.replace(/(.+?)(?:page)?\.vue$/i, (m, p) => p.toLowerCase());
		const rootPage = normalizePage(rootPageName);

		return pages.reduce<string[]>((routes, page) => {
			if (!page.endsWith('.vue')) {
				return routes;
			}

			const name = normalizePage(page);
			const route = name === rootPage ? '/' : `/${name}`;

			routes.push(route);

			return routes;
		}, []);
	}

	/**
	 * Replace masks in html with content
	 */
	private static replaceMasks(template: string, data: Record<string, string>): string {
		for (const [mask, content] of Object.entries(data)) {
			template = template.replace(`<!--[${mask}]-->`, content);
		}

		return template;
	}
}
