import {constants} from 'node:fs';
import {access} from 'node:fs/promises';
import {join} from 'node:path';
import {cwd} from 'node:process';

import express from 'express';

/**
 * Test server to check SSG pages
 */
const root = cwd();
const app = express();
const dist = 'dist-ssg';
const port = 3000;

app.use('/assets', express.static(`${dist}/assets`));
app.use(['/:page', '/'], async (req, res) => {
	const {page = 'index'} = req.params;
	const path = join(root, `${dist}/${page}.html`);

	try {
		await access(path, constants.F_OK);
		res.sendFile(path);
	} catch (e) {
		res.sendStatus(404);
	}
});

app.listen(port, () => console.log(`The server is running at:\n\nhttp://localhost:${port}`));
