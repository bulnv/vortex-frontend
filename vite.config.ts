import {sharedConfig} from './vite.config.shared';

/**
 * The main config for SPA
 */
export default sharedConfig;
